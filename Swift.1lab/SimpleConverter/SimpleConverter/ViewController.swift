//
//  ViewController.swift
//  SimpleConverter
//
//  Created by Admin on 13.02.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var inputValueInBytes: UITextField!
    
    @IBOutlet weak var outputValueInBits: UILabel!

    @IBOutlet weak var outputValueInMegabytes: UILabel!
    
    @IBOutlet weak var outputValueInGigabytes: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onConvertButtonClick(_ sender: Any) {
        
        if let valueInBytes = Double(inputValueInBytes.text!) {
           let converter = ByteConverter()
            
            let valueInBits = converter.convertValueInBytesToBits(valueInBytes: valueInBytes)
            let valueInMegabytes = converter.convertValueInBytesToMegabytes(valueInBytes: valueInBytes)
            let valueInGygabytes = converter.convertValueInBytesToGigabytes(valueInBytes: valueInBytes)
            
            outputValueInBits.text = "Value in bits: " + String(valueInBits)
            outputValueInMegabytes.text = "Value in megabytes: " + String(valueInMegabytes)
            outputValueInGigabytes.text = "Value in gigabytes: " + String(valueInGygabytes) 
        } else{
            showAlert()
        }
        
    }
    
    @IBAction func showAlert(){
        let alertController = UIAlertController(title: "Incorrect input", message: "Please, verify input value.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(alertAction)
            
            present(alertController, animated: true, completion: nil)
    }

}

